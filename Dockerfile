FROM scratch
WORKDIR /tmp

COPY --from=busybox /bin/cp /bin/cp
ENTRYPOINT ["/bin/cp", "-up"]
CMD [".env.example", ".env"]
